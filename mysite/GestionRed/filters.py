from django import template
from MetodosPython.DBInternalAccess import DBAccess
register = template.Library()
@register.filter(name='registeredDevicesForUser')
def registeredDevicesForUser(value):
    registered=DBAccess.getRegisterForUser(value)
    return registered.network_element.objects.all()
