from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import User,Group

# Create your models here.
class ElementoDeRed(models.Model):
    IP_Address=models.CharField(max_length=200)
    MAC_Address=models.CharField(max_length=200)
    offset_bytes=models.CharField(max_length=200)
    transfered_bytes=models.CharField(max_length=200)
    time=models.CharField(max_length=200)
    previous_bytes=models.CharField(max_length=200)
    previous_previous_bytes=models.CharField(max_length=200)
class HistoricoDeConsumo(models.Model):
    dia=models.CharField(max_length=200)
    consumo=models.CharField(max_length=200)
    IP_Address=models.CharField(max_length=200)
    MAC_Address=models.CharField(max_length=200)
class LogDeLaRed(models.Model):
    time=models.CharField(max_length=200)
    message=models.CharField(max_length=200)
    information=models.CharField(max_length=500)
class NAC(models.Model):
    srcmac=models.CharField(max_length=200)
    dstmac=models.CharField(max_length=200)
    srcip=models.CharField(max_length=200)
    dstip=models.CharField(max_length=200)
    srcport=models.CharField(max_length=200)
    dstport=models.CharField(max_length=200)
    ethtype=models.CharField(max_length=200)
    protocol=models.CharField(max_length=200)
    inport=models.CharField(max_length=200)
class Register(models.Model):
    user=models.ForeignKey(User)
    network_element=models.ManyToManyField(ElementoDeRed)
    registered_date=models.DateTimeField(null=True,blank=True)