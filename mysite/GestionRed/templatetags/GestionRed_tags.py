from django import template
from MetodosPython.DBInternalAccess import DBAccess
from django.template.defaultfilters import stringfilter
register = template.Library()
@register.filter(name='registeredDevicesForUser')
@stringfilter
def registeredDevicesForUser(value):
	user=DBAccess().getUserByUsername(value)
	registered=DBAccess().getRegisterForUser(user)
	if registered is not 1:
		return registered.network_element.all()
	else:
		return []