from django.http import HttpResponse,HttpResponseRedirect
from django.template import Template, Context
from django.template.loader import get_template
from django.contrib.auth.models import User
from django.contrib import auth
from MetodosPython.NetworkApps.NACApp import NAC
from MetodosPython.DBInternalAccess import DBAccess
from MetodosPython.NetworkFunctions_RESTAPI import prepareDataForSearchForm
from django.contrib.auth import logout,login

'''Initial Page'''
def Inicio(request):
	template=get_template('Inicio.html')
	inicio=template.render(Context({"ip_list":1}))
	return HttpResponse(inicio)
def createUsers(request):
	"""(request)->html
	Handles creating users
	"""
	if request.method=='POST':
		username=request.POST['username']
		password=request.POST['password']
		email=request.POST['email']
	DBAccess().createUser(username,password,email)
	DBAccess().registerDeviceForUser(username,1)
	template=get_template('Inicio.html')
	inicio=template.render(Context({"ip_list":1}))
	return HttpResponse(inicio)
def authentication(request):
	"""(request)->html
	Handles authentication
	"""
	if request.method=='POST':
		username=request.POST['user']
		password=request.POST['password']
		ip=request.META['REMOTE_ADDR']
		user = auth.authenticate(username=username, password=password)
		if user is not None:
			login(request, user)
			mac=DBAccess().isNetworkDeviceIP(ip)
			if not mac=='El dispositivo no se encuentra en la red':
				NAC().registration(username,mac)
			#Obtenemos MAC de nuestra base de datos
			t=get_template('search_form.html')
			device_tuple,user_list=prepareDataForSearchForm()
			html=t.render(Context({"device_dict":device_tuple,"user_list":user_list}))
			return HttpResponse(html)
	template=get_template('Inicio.html')
	inicio=template.render(Context({"registration_failed":1}))
	return HttpResponse(inicio)
def logOut(request):
	"""(request)->html
	Log out
	"""
	logout(request)
	template=get_template('Inicio.html')
	inicio=template.render(Context({"logout_successful":1}))
	return HttpResponse(inicio)
def settings(request):
	"""(request)->html
	Handles settings page
	"""
	group_list=DBAccess().getGroups()
	user_list=DBAccess().getUsers()
	register_list=DBAccess().getRegister()
	template=get_template('settings.html')
	html=template.render(Context({"user_list":user_list,"group_list":group_list,"register_list":register_list}))
	return HttpResponse(html)
def groupUserAssign(request):
	"""
	Changes group for user
	"""
	DBAccess().changeUserGroup(request.POST['username'],request.POST['group'])
	return settings(request)
def register(request):
	"""(request)->html
	Register page
	"""
	template=get_template('register.html')
	register=template.render(Context({"ip_list":1}))
	return HttpResponse(register)