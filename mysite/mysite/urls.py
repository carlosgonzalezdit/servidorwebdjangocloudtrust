from django.conf.urls import patterns, url, include

from MetodosPython.NetworkFunctions_RESTAPI import changeUserGroup,search_form,filter,not_filter,history, log, ipMobile, logMobile, enableMobile,filterMobile,authToUser
from mysite.views import Inicio,authentication,logOut,createUsers,settings,groupUserAssign,register
urlpatterns = patterns('',
                       url(r'^$', Inicio),
                       url(r'^createUsers/',createUsers),                     
                       url(r'^autentica/',authentication),                     
                       url(r'^search-form/$',search_form),
                       url(r'^filtra/$',filter),
                       url(r'^no_filtra/$',not_filter),
                       url(r'^historico/$',history),
                       url(r'^log/',log),
                       url(r'^ipMovil/',ipMobile),
                       url(r'^logMovil/',logMobile), 
                       url(r'^EnableMovil/',enableMobile),
                       url(r'^filtraMovil/',filterMobile),                     
                       url(r'^autenticaUser/',authToUser),     
                       url(r'^settings/',settings), 
                       url(r'^asignaGrupoUser/',groupUserAssign),
                       url (r'^create_user/',register),                                                          
                       url (r'^log_out/',logOut),      
                      url (r'^change_user_group/',changeUserGroup),                                                        
                       )
