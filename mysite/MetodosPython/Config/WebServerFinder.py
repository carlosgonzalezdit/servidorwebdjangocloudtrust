#!usr/bin/python
#------------------------------------------------------------------
#Filename:WebServerFinder.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:30/04/2014
#------------------------------------------------------------------
from MetodosPython.REST_Plugins.RESTAPI import RESTAPI
from MetodosPython.REST_Plugins.FloodlightRESTAccess import FloodlightRESTAccess

class WebServerFinder(object):
	"""Finds Openflow web server port"""
	def __init__(self):
		pass
	def findWebServerPort(self):
		"""()->int
		Finds and returns the openflow port number of the web server
		"""
		rest_access=FloodlightRESTAccess()
		#Obtain switches list from the controller
		switches_dict=rest_access.getSwitchesJSON()
		#Parse and search WebServer port
		port=rest_access.searchPortByName(switches_dict,name='ServidorWeb-e1')
		#Returns port
		print port[1]
		return port[1]
	def findPort(self,interfaceName):
		"""()->int
		Finds and returns the interfaceName port number
		"""
		rest_access=FloodlightRESTAccess()
		#Obtain switches list from the controller
		switches_dict=rest_access.getSwitchesJSON()
		#Parse and search WebServer port
		port=rest_access.searchPortByName(switches_dict,name=interfaceName)
		#Returns port
		return port[1]