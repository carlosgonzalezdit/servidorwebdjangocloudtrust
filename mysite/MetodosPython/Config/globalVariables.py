from MetodosPython.Config.WebServerFinder import WebServerFinder

def init():
	global web_server_port
	web_server_port=WebServerFinder().findWebServerPort()