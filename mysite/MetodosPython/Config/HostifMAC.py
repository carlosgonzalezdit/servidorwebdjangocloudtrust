import commands
import re
import os
from os import remove, close 
from shutil import move
config_file=os.getcwd()+'/Settings.conf'
def searchMAC(interface_name,config_string):
	mac_pattern='([a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2})'
	pattern=interface_name+'[\s]*Link encap:Ethernet[\s]*'+'HWaddr[\s]*'+mac_pattern
	mac_address=re.search(pattern,config_string)
	return mac_address.group(1)

def changeHostifMAC(mac):
	mac_pattern='([a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}){0,1}'
	hostif_pattern="hostif_MAC=\['"+mac_pattern+"'\]"
	config_file_descriptor=open(config_file,'r+')
	configuration=config_file_descriptor.read()
	replacement_string="hostif_MAC=['"+mac+"']"
	modified_string=re.sub(hostif_pattern, replacement_string, configuration)
	return modified_string
	config_file_descriptor.write(modified_string)

def updateConfigFile(new_config,old_config_file):
	new_file = open("archivoTemporal.txt",'w')
	new_file.write(new_config)
	new_file.close()
	move(old_config_file,'last_known_config.txt')
	move('archivoTemporal.txt',old_config_file)

ifconfig=commands.getoutput('ifconfig')
mac=searchMAC('Net0',ifconfig)
new_configuration=changeHostifMAC(mac)
updateConfigFile(new_configuration,config_file)