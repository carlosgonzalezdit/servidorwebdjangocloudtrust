#!usr/bin/python
#------------------------------------------------------------------
#Filename:DBInternalAccess.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	DB Access. 
	Provides save access to the DB
"""
#------------------------------------------------------------------
import os,sys,math,json,time,socket
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
from GestionRed.models import ElementoDeRed, HistoricoDeConsumo, LogDeLaRed,NAC,Register
from django.contrib.auth.models import User,Group
from django.db import connection
"""Tool to obtain network information"""
class DBAccess(object):
	"""
	Contains all access to the DB
	"""
	def __init__(self,json_information=dict(),counter=int(1)):
		self.json_information=json_information
		self.counter=counter
		self.event_type=dict(filter='filter')
		self.message_value=dict(filtered='filtered',not_filtered='not_filtered')
		self.history_matrix=[]
		connection.close()

	def saveLog(self,time,message,information):
		"""(date,str,str)->void
		Saves log info
		"""
		log=LogDeLaRed(time=time,
						message=message,
						information=information) #Register the event in the log system
 		log.save()
 	def createNetworkElement(self,MAC_Address,IP_Address,transfered_bytes,previous_bytes,previous_previous_bytes,time,offset_bytes):
	 	"""
	 	Creates NetworkElement
	 	"""
 		new_Device=ElementoDeRed(MAC_Address=MAC_Address,
 			    						IP_Address=IP_Address,
 			    						transfered_bytes=0,
 			    						previous_bytes=0,
 			    						previous_previous_bytes=0,
 			    						time=0,
 			    						offset_bytes=0)
 		new_Device.save()
 	def getNetworkElementByMAC(self,MAC_Address):
	 	"""(str)->str/element
	 	Gets specific device for a MAC
	 	"""

 		try:
 			element=ElementoDeRed.objects.get(MAC_Address=MAC_Address)
 			return element
 		except ElementoDeRed.DoesNotExist:
 			return "Does not exist"
 	def updateNetworkElementsDate(self,date):
	 	"""(date)->void
	 	Updates NetworkElements Dates
	 	"""

 		for element in ElementoDeRed.objects.all():	#For every device in the network, update their last info received data
		    element.time=date
		    element.save()
	def updateNetworkElementIP(self,networkElement,IP_Address):
		"""(NetworkElement,string)->void
		Update Network Element IP
		"""
		networkElement.IP_Address=IP_Address
		networkElement.save()
	def isNetworkDeviceIP(self,ip):
		"""(str)->str
		Searches a device in the network
		"""
		try:
		    device=ElementoDeRed.objects.get(IP_Address=ip)
		except ElementoDeRed.DoesNotExist:
		    return "El dispositivo no se encuentra en la red"
		else:
			print device
			return device.MAC_Address
	def getNetworkDevices(self):
		"""(void)->list
		Return network devices
		"""
		return ElementoDeRed.objects.all()
	def saveHistory(self,dia,MAC_Address,consumo):
		"""(date,str,str)->void
		Snapshot of network elements 
		"""

		h=HistoricoDeConsumo(dia=dia,
							MAC_Address=MAC_Address,
							consumo=consumo)
		h.save()		

	def active_device(self,MAC):
		"""(str)->str
		Search and returns if the device is currently active
		"""	
		try:
		    historico=ElementoDeRed.objects.get(MAC_Address=MAC)
		except HistoricoDeConsumo.DoesNotExist:
		    return "Activo"
		else:
		    if historico.previous_bytes>=historico.transfered_bytes:
		        if historico.previous_previous_bytes==historico.transfered_bytes:
		            return "Inactivo"
		        else:
		            return "Inactivo"
		    elif historico.previous_bytes<historico.transfered_bytes:
		        return "Activo"
	def log(self,request):
		"""(httpRequest)->list
		Returns network log
		"""
		log=[]
		if 'q' in request.GET:
			q=request.GET['q']
			#Order log
			if request.path=='/log/ordena/':
			    #By time
			    if q=='time':
			        ordered_log=LogDeLaRed.objects.order_by('-time')
			    #By event
			    elif q=='event':
			        ordered_log=LogDeLaRed.objects.order_by('-message')
			    #Do not order
			    else:
			        ordered_log=LogDeLaRed.objects.all()
			#Show only specific messages
			elif request.path=='/log/filtra/':
			    ordered_log=LogDeLaRed.objects.filter(message=q)
			else:
			    ordered_log=LogDeLaRed.objects.all()
		else:
		    ordered_log=LogDeLaRed.objects.all()
		#Format por the template
		for object in ordered_log:
		    log.append((object.time,object.message,object.information))
		return log

	def JSONnetworkElements(self):
		"""()->dict
		Returns JSON with network elements
		"""
		self.json_information=dict()
		#Prepare JSON message with the devices in the network
		network_elements=ElementoDeRed.objects.all()
		for element in network_elements:
			#Device is active or not
		    activity=self.active_device(element.MAC_Address)
		    node=dict(IP_Address=element.IP_Address,MAC_Address=element.MAC_Address, transfered_bytes=element.transfered_bytes,time=element.time,activity=activity)
		    string= 'elemento'+str(self.counter)
		    self.json_information[string]=node
		    self.counter=self.counter+1
		return self.json_information

	def networkElements(self):
		"""()->list
		Returns list of MAC Addresses
		"""	
		network_elements=ElementoDeRed.objects.all()
		elements=[]
		for element in network_elements:
			#Device is active or not
			elements.append((element.IP_Address,element.MAC_Address))
		return elements
	def networkLog(self):
		"""()->dict
		Returns JSON with network information through time
		"""    
		self.json_information=dict()
		log=LogDeLaRed.objects.order_by('-time')[0:20]
		for event in log:
		    one_event=dict(time=event.time,message=event.message,information=event.information)
		    string='evento'+str(self.counter)
		    self.json_information[string]=one_event
		    self.counter=self.counter+1
		return self.json_information

	def historyMatrix(self):
		"""()->list
		Returns a matrix containing each device in the network and their consumed traffic among the time
		"""	
		lista_fila=[]
		lista_MAC=[]
		lista_historico=[]
		#Obtain last 100 results, elmininates duplicate dates and order it
		for historico in HistoricoDeConsumo.objects.order_by('-dia')[0:100]:
		    lista_historico.append(int(historico.dia))
		times=list(set(lista_historico))
		times.sort()
		#List all devices
		for object in ElementoDeRed.objects.all():
		    lista_MAC.append(str(object.MAC_Address))
		#For each date
		for objeto in times:
			#Fill de time row of the table(first one)
		    lista_fila.append(objeto)
	    	#For each date, if the device exists fill the colum with the consumed traffic, otherwise fill it with 0
		    for MAC in lista_MAC:
		        try:
		            objeto_historico= HistoricoDeConsumo.objects.get(MAC_Address=MAC,dia=objeto)
		        except HistoricoDeConsumo.DoesNotExist:
		            lista_fila.append(0)
		        else:
		            lista_fila.append(int(objeto_historico.consumo))
		            continue
		    self.history_matrix.append(lista_fila)
		    lista_fila=[]
		#Insert the MAC row to make a table
		lista_MAC.insert(0,'Minutes')
		self.history_matrix.insert(0,lista_MAC)
		return self.history_matrix
	def saveInfoNACDevice(self,srcmac,dstmac,srcip,dstip,srcport,dstport,ethtype,protocol,inport):
		"""
		Registers a device in the NAC database
		"""
		nac_entry=NAC(srcmac=srcmac,
					dstmac=dstmac,
					srcip=srcip,
					dstip=dstip,
					srcport=srcport,
					dstport=dstport,
					ethtype=ethtype,
					protocol=protocol,
					inport=inport)
		nac_entry.save()
	"""Looks for a device in the NAC database"""
	def getNACEntriesForDevice(self,srcmac):
		entries=NAC.objects.filter(srcmac=srcmac)
		return entries
	"""NAC entry exists"""
	def nacEntryExists(self,srcmac,dstmac,srcip,dstip,srcport,dstport,ethtype,protocol,inport):
		if NAC.objects.filter(srcmac=srcmac,dstmac=dstmac,srcip=srcip,dstip=dstip,srcport=srcport,dstport=dstport,ethtype=ethtype,protocol=protocol,inport=inport).exists()==True:
			return 0
		else:
			return 1
	"""Deletes device from the NAC database"""
	def deleteEntriesForDevice(self,srcmac):
		entries=NAC.objects.filter(srcmac=srcmac)
		entries.delete()
	"""Registers a device with a specified group"""
	def registerDeviceForUser(self,username,mac):
		try:
			user=User.objects.get(username=username)
			network_element=ElementoDeRed.objects.get(MAC_Address=mac)
			user_exists=Register.objects.get(user=user)
			if not network_element in user_exists.network_element.all():
				user_exists.network_element.add(network_element)
		except User.DoesNotExist:
			print "Usuario no existe"
			return
		except Register.DoesNotExist:
			print "no existe el user en el register, lo creo"
			register=Register(user=user)
			register.save()
			register.network_element.add(network_element)
		except ElementoDeRed.DoesNotExist:
			print "elemento de red no existe"
			return
	def userRegisteredForMAC(self,mac):
		"""(str)->Register/1
		Searches if a device belongs to a user
		"""
		try:
			element=ElementoDeRed.objects.get(MAC_Address=mac)
			register=Register.objects.get(network_element=element)
			return register
		except ElementoDeRed.DoesNotExist:
			return 1
		except Register.DoesNotExist:
			print "elemento no registrado"
			return 1
	def undefineRegisteredDevice(self,mac):
		"""(str)->Register
		Undefine Device For Register
		"""
		try:
			element=ElementoDeRed.objects.get(MAC_Address=mac)
			register=Register.objects.get(network_element=element)
			register.network_element.remove(element)
		except Register.DoesNotExist:
			return 1
	def createGroups(self):
		"""()->void
		Defines Groups
		"""
		if not Group.objects.all():
			Group(name="Guest").save() 
			Group(name="NotValidated").save() 
			Group(name="User").save() 
			Group(name="Admin").save() 
	def getGroups(self):
		"""(void)->list
		Returns a list with the existent groups
		"""
		return Group.objects.all()
	def createUser(self,username,password,email,group='NotValidated'):
		"""(str,str,str,str)->void
		Creates a user in the database
		"""
		try:
			User.objects.get(username=username)
			print "ya existe"
		except User.DoesNotExist:
			user = User.objects.create_user(username=username,email=email,password=password)
			user.save()
			self.changeUserGroup(username,group)
	def changeUserGroup(self,username,groupname):
		"""(str,str)->void
		Changes de group for an user
		"""
		try:
			user=User.objects.get(username=username)
			group=Group.objects.get(name=groupname)
			user.groups.clear()
			user.groups.add(group) 
		except User.DoesNotExist:
			print "Usuario no existe"
	def getUserByUsername(self,username):
		""" (str)->User
		Gets user by username
		"""
		try:
			return User.objects.get(username=username)
		except User.DoesNotExist:
			return 1
	def getUsers(self):
		"""()->list
		Returns a list of users
		"""
		try:
			return User.objects.all()
		except User.DoesNotExist:
			return 1
	def getRegisterForUser(self,user):
		""" (User)->Register
		Returns a Register row for a specified User
		"""
		try:
			return Register.objects.get(user=user)
		except Register.DoesNotExist:
			return 1
	def updateRegisterDateForUser(self,register,date):
		"""(Register,datetime)->void
		Updates the register date for an user
		"""
		register.registered_date=date
		register.save()
	def getRegister(self):
		"""()->void
		Returns all registered user
		"""
		return Register.objects.all()
	def removeRegisterElement(self,register):
		"""(Register)->void
		Removes registered element
		"""
		register.delete()