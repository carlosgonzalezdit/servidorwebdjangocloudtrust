from zope.interface import Interface
class ITrafficCounterService(Interface):
	""" Interface for the new devices service"""
	def trafficInfoMessage(message):
		"""
		This method will be called when a new message with consumed data arrive
		"""