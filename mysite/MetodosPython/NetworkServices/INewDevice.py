from zope.interface import Interface
class INewDeviceService(Interface):
	""" Interface for the new devices service"""
	def newDeviceDetected(message):
		"""
		This method will be called when a new device is detected in the network
		"""