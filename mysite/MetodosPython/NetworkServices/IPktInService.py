from zope.interface import Interface
class IPktInService(Interface):
	""" Interface for the pktIn Messages"""
	def pktInMessage(message):
		"""
		This method will be called when a new pktIn arrives to the controller from the OVS
		"""