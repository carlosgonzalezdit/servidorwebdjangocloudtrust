
#!usr/bin/python
#------------------------------------------------------------------
# Filename:ControllerInfo.py
# Author:Carlos Gonzalez Merino
# email:carlosgonza@dit.upm.es
# Date:22/04/2014
#------------------------------------------------------------------
"""
	controller information.
	Contains network connections info
"""
#------------------------------------------------------------------
import os
global global_variables
global_variables = {}
config_file = '/home/ServidorWebDjango/servidorwebdjangocloudtrust/mysite/MetodosPython/Config/Settings.conf'
execfile(config_file, global_variables)


class ControllerInfo(object):


    """
    Contains all controller information relative to connections
    """

    def __init__(self, controller_type="floodlight"):
        self.offset = 0
        self.web_server = global_variables['web_server_ip']
        if global_variables['controller_type'] == 'pyretic':
            self.is_pyretic_controller()
        if global_variables['controller_type'] == 'floodlight':
            self.is_floodlight_controller()
        self.controller_ip = global_variables['controller_ip']
        self.controller_type = global_variables['controller_type']

    def is_pyretic_controller(self, devices_port=60000, traffic_port=60001, filter_port=50003, comunication_port=60002, controller_type='pyretic'):
        """
        Sets data for pyretic controller
        """
        self.devices_port = devices_port
        self.traffic_port = traffic_port
        self.filter_port = filter_port
        self.comunication_port = comunication_port

    def is_floodlight_controller(self, devices_port=60000, traffic_port=60001, filter_port=8080, comunication_port=60002, controller_type='floodlight'):
        """
        Sets data for floodlight controller
        """
        self.devices_port = devices_port
        self.traffic_port = traffic_port
        self.filter_port = filter_port
        self.comunication_port = comunication_port


class captivePortal(object):

    """
    Contains connection info for the captive portal
    """

    def __init__(self):
        self.mac = global_variables[
            'captive_portal_mac']  # 'c8:bc:c8:a2:9d:13'
        self.ip = global_variables['captive_portal_ip']
        self.port = global_variables['captive_portal_port']


class controlDevices(object):

    """
    Specifies special devices that wont appear to the user
    """

    def __init__(self):
        pass

    def ishostif(self, ip):
        """
        Checks if it is the hostif
        """
        if (ip == '10.0.0.8'):
            return 1
        return 0

    def isCaptivePortal(self, mac):
        """
        Checks if it is the Captive Portal
        """
        if (mac == global_variables['captive_portal_mac']):
            return 1
        return 0

    def isControlDevice(self, mac):
        """
        Checks if it is a control device
        """
        mac_control_list = global_variables['mac_control_list']
        for device in mac_control_list:
            if (mac == device):
                return 1
        return 0

    def isBroadcast(self, mac):
        """
        Checks if it is a broadcast dir
        """
        broadcast_dir = 'ff:ff:ff:ff:ff:ff'
        if (mac == broadcast_dir):
            return 1
        return 0

    def getControlDevicesMac(self):
        """
        Return mac control list
        """
        return global_variables['mac_control_list']

    def getEventSenderList(self):
        """
        Return event sender black list
        """
        return global_variables['event_sender_list']
