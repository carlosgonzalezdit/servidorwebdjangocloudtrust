
#!usr/bin/python
#------------------------------------------------------------------
#Filename:ControllerReceiver.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Notification Receiver. 
	Receives and handles notifications
"""
#------------------------------------------------------------------

from multiprocessing import Process, Queue
import os,sys,math,json,time,socket
from MetodosPython.NetworkApps.TrafficCounterApp import TrafficCounter
from MetodosPython.NetworkApps.DeviceDetecterApp import DeviceDetecter
from MetodosPython.NetworkApps.NACApp import NAC,SessionHandler
from MetodosPython.DBInternalAccess import DBAccess
from django.conf import settings
"""Class delegate for all sockets communications with the controller"""
class NotificationReceivers(object):
	def __init__(self,controller):
		self.controller=controller
		self.registerAppForService()
	def registerAppForService(self):
		self.new_device_service_apps_list=[DeviceDetecter()]
		self.traffic_counter_service_apps_list=[TrafficCounter()]
		self.pkt_in_service_apps_list=[NAC()]
	"""Starts receivers"""
	def start(self):
		p=Process(target=self.NewDeviceDetecterService,name='NewDeviceDetecter')
		p.start()
		p1=Process(target=self.TrafficCounter,name='ContadorTrafico')
		p1.start()
		p2=Process(target=self.pktIn,name='pktInMessage')
		p2.start()
	def NewDeviceDetecterService(self):
		print "Hebra detectora de dispositivos"
		self.conexionCreator(self.controller.devices_port,'NewDeviceDetecter')
	def TrafficCounter(self):
	    print "Hebra contadora trafico"
	    self.conexionCreator(self.controller.traffic_port,'TrafficCounter')
	def pktIn(self):
		print "Hebra para los pkt In"
		self.conexionCreator(self.controller.comunication_port,'pktInMessage')
	def conexionCreator(self,port,accion):
	    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	    s.bind(('',int(port)))
	    s.listen(5)
	    while 1:
	    	data=""
	        conn,addr=s.accept()
	        while 1:
	            data=conn.recv(2048)
	            if not data:break
	            dictionary=json.loads(data)
	            self.handler(accion,dictionary)
	def handler(self,accion,dictionary):
	 	if accion=='NewDeviceDetecter':
	 		for app in self.new_device_service_apps_list:
	 			app.newDeviceDetected(dictionary)
	 	elif accion=='TrafficCounter':
	 		for app in self.traffic_counter_service_apps_list:
	 			app.trafficInfoMessage(dictionary)
	 	elif accion=='pktInMessage':
	 		for app in self.pkt_in_service_apps_list:
	 			app.pktInMessage(dictionary)