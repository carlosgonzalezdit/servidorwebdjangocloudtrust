#!usr/bin/python
#------------------------------------------------------------------
#Filename:ControllerDispatcher.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Controller Dispatcher. 
	Maps network apps to their specific controller
"""
#------------------------------------------------------------------

from MetodosPython.REST_Plugins.FloodlightRESTAccess import FloodlightRESTAccess
from MetodosPython.ControllerInfo import ControllerInfo
from MetodosPython.REST_Plugins.RESTAPI import RESTAPI
from MetodosPython.Socket_Plugins.JSONSockets import JSONSocketHandler
from MetodosPython.Middleware.FloodlightMiddleware import HistoryMiddleware
from MetodosPython.DBInternalAccess import DBAccess
class ControllerDispatcher(object):
	"""docstring for ControllerDispatcher"""
	def __init__(self):
		self.controller=ControllerInfo()
		if self.controller.controller_type=='floodlight':
			self.rest=RESTAPI(self.controller)								#general rest object								
			self.rest_floodlight=FloodlightRESTAccess()						#specific floodlight object
	"""When the system starts, enable middleware and initial data necessary in db"""
	def startDispatcher(self):
		"""
		Initial configuration for ControllerDispatcher
		"""
		DBAccess().createGroups()
		self.middleware()
	def middleware(self):
		"""
		Based on the controller starts middleware
		"""
		if self.controller.controller_type=='floodlight':
			HistoryMiddleware().start()
	def set_payload(self,app,inport=None,srcmac=None,dstmac=None,srcip=None,dstip=None,tos=None,srcport=None,dstport=None,ethtype=None,protocol=None,vlan_id=None,vlan_pcp=None,priority=None):
		"""
		Payload for the messages to be sent to the controller
		"""
		self.message_payload= dict(inport=inport,    \
		              srcmac=srcmac,    \
		              dstmac=dstmac,    \
		              srcip=srcip,     \
		              dstip=dstip,     \
		              tos=tos,       \
		              srcport=srcport,   \
		              dstport=dstport,   \
		              ethtype=ethtype,   \
		              protocol=protocol,  \
		              vlan_id=vlan_id,   \
		              vlan_pcp=vlan_pcp)
		self.priority=priority
		self.app=app

	def addDeviceToSwitchCommodity(self):
		"""
		Add the specified device in the payload to the switch commodity
		"""
		if self.controller.controller_type=='pyretic':
			pass
		elif self.controller.controller_type=='floodlight':
			srcmac='00:00:'+self.message_payload['srcmac']
			self.rest_floodlight.set_payload(self.message_payload,
										self.app,self.priority)
			self.rest.setPath(self.rest_floodlight.addCommoditySwitchPath(srcmac))
			self.rest_floodlight.messagessForAllSwitches() #Sending message for all discovered switches	
			self.rest.post()
	def deleteDeviceToSwitchCommodity(self):
		"""
		Delete the specified device in the payload to the switch commodity
		"""
		if self.controller.controller_type=='pyretic':
			pass
		elif self.controller.controller_type=='floodlight':
			srcmac='00:00:'+self.message_payload['srcmac']
			self.rest_floodlight.set_payload(self.message_payload,
										self.app,self.priority)
			self.rest.setPath(self.rest_floodlight.deleteCommoditySwitchPath(srcmac))
			self.rest_floodlight.messagessForAllSwitches() #Sending message for all discovered switches	
			self.rest.get()	
	def addDeviceToPktInServiceBlackList(self):
		"""
		Add the specified device in the payload to the black list for the device notification
		"""	
		if self.controller.controller_type=='pyretic':
			pass
		elif self.controller.controller_type=='floodlight':
			srcmac='00:00:'+self.message_payload['srcmac']
			self.rest_floodlight.set_payload(self.message_payload,
										self.app,self.priority)
			self.rest.setPath(self.rest_floodlight.DeviceNotNotificablePath(srcmac))
			self.rest_floodlight.messagessForAllSwitches() #Sending message for all discovered switches	
			self.rest.post()
	def deleteDeviceToPktInServiceBlackList(self):
		"""
		Add the specified device in the payload to the black list for the device notification
		"""		
		if self.controller.controller_type=='pyretic':
			pass
		elif self.controller.controller_type=='floodlight':
			srcmac='00:00:'+self.message_payload['srcmac']
			self.rest_floodlight.set_payload(self.message_payload,
										self.app,self.priority)
			self.rest.setPath(self.rest_floodlight.deleteCommoditySwitchPath(srcmac))
			self.rest_floodlight.messagessForAllSwitches() #Sending message for all discovered switches	
			self.rest.get()
	def sendOrder(self,action,info=None):
		"""
		Sends message to floodlight/pyresonance controller 
		"""
		#Mapping to the specific controller
		if self.controller.controller_type=='pyretic':
			message_type='state'										#Pyretic needs message_type=state to modify policy
			if self.app=='filter':
				if action=='drop':
					message_value='filtered'							
				json_object=JSONSender(self.controller)
				message=json_object.set_payload(self.message_payload)	#Set payload for message
				message.set_event('filter',message_type,message_value)	#Set event for message
				message.send()
		#if controller is floodlight
		elif self.controller.controller_type=='floodlight':
			rest=RESTAPI(self.controller)								#general rest object								
			rest_floodlight=FloodlightRESTAccess()						#specific floodlight object
			rest_floodlight.set_payload(self.message_payload,
										self.app,self.priority)
			#Different apps need different resources
			if (self.app=='NAC')|(self.app=='filter')|(self.app=='DNSAllower'):
				rest.setPath(rest_floodlight.flowpusherPath())
				if action=='drop':
					#If drop only set action to output					
					rest_floodlight.set_action('output')
				elif action=='set':				#More than one field can be set
					for dic in info:			#info contains all the fields to be set
						for field in dic:
							rest_floodlight.set_action(field,
														dic[field])
				rest_floodlight.messagessForAllSwitches() #Sending message for all discovered switches	
				for payload in rest_floodlight.payload_list:				
					rest.post(payload)
	def deleteOrder(self):
		"""
		Sends a message to delete an installed order
		"""
		#Needed differentiation between pyretic and floodlight for the same service
		if self.controller.controller_type=='pyretic':
			message_type='state'
			if self.app=='filter':
				message_value='not_filtered'
			message=JSONSender(self.controller).set_payload(self.message_payload)
			message.set_event(event_type,message_type,message_value)
			message.send()
		#Floodlight deletes orders by their name
		elif self.controller.controller_type=='floodlight':
			rest=RESTAPI(self.controller)
			rest_floodlight=FloodlightRESTAccess()
			rest_floodlight.set_payload(self.message_payload,self.app,self.priority)	#We construct the order name with the implied fields
			if (self.app=='NAC')|(self.app=='filter')|(self.app=='DNSAllower'):
				rest_floodlight.messagessForAllSwitches()								#Message for all discovered switches
				rest.setPath(rest_floodlight.flowpusherPath())								#Sets REST setPath for floodlight
				for payload in rest_floodlight.payload_list:
					rest.remove(payload)								#Removes order from floodlight
