
#!usr/bin/python
#------------------------------------------------------------------
#Filename:FloodlightRESTAccess.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
    Floodlight REST. 
    Manages the access to the floodlight REST API
"""
#------------------------------------------------------------------
import httplib
import json
from MetodosPython.REST_Plugins.RESTAPI import RESTAPI
from MetodosPython.ControllerInfo import ControllerInfo

"""Manages static flows to floodlight controller via API REST"""
class FloodlightRESTAccess(object):
    def __init__(self):
        self.payload=dict()
        self.payload_list=[]
    """Gets all the switches in the network"""
    def getSwitchesJSON(self):
        rest=RESTAPI(ControllerInfo('floodlight'))
        rest.setPath('wm/core/controller/switches/json')
        switches_json=rest.get()[2]
        switches_list=json.loads(switches_json)
        return switches_list
    def getSwitchesInScenario(self):
        """()->list
        Returns switch ports info
        """
        switches_list=self.getSwitchesJSON()
        switches_info=[]
        for switches_dict in switches_list:
            switches_info.append(switches_dict['dpid'])
        return switches_info
    def searchPortByName(self,switches_json,name):
        """(dict,str)->tuple(str,int)
        Searches a openflow port by name in all switches
        """
        for switch in switches_json:
            for port in switch['ports']:
                if port['name']==name:
                    return (name,port['portNumber'])
        return (1,1)
    """Path to push a flow"""
    def flowpusherPath(self):
        return '/wm/staticflowentrypusher/json'
    """Path to retrieve stats"""
    def switchstatisticsPath(self):
        return '/wm/core/switch/all/flow/json'
    def addCommoditySwitchPath(self,mac):
        """Path to commodity switch add device"""
        return '/wm/learningswitch/switch/'+mac+'/add/json'
    def deleteCommoditySwitchPath(self,mac):
        """Path to commodity switch delete device"""
        return '/wm/learningswitch/switch/'+mac+'/delete/json'
    def DeviceNotNotificablePath(self,mac):
        """Path to specify a non notificable device """
        return '/wm/learningswitch/switch/noEventSender/'+mac+'/add/json'
    """Transforms payload message with pyretic format to floodlight format"""
    def set_payload(self,payload,name,priority):
        self.payload['name']=name   #Name for the flow is build based on all the matching fields
        if payload['srcmac']:
            self.payload['name']+='-'+payload['srcmac']
            self.payload['src-mac']=payload['srcmac']
        if payload['dstmac']:
            self.payload['name']+='-'+payload['dstmac']
            self.payload['dst-mac']=payload['dstmac']
        if payload['srcip']:
            self.payload['name']+='-'+payload['srcip']
            self.payload['src-ip']=payload['srcip']
        if payload['dstip']:
            self.payload['name']+='-'+payload['dstip']
            self.payload['dst-ip']=payload['dstip'] 
        if payload['srcport']:
            self.payload['name']+='-'+payload['srcport']
            self.payload['src-port']=payload['srcport'] 
        if payload['dstport']:
            self.payload['name']+='-'+payload['dstport']
            self.payload['dst-port']=payload['dstport'] 
        if payload['inport']:
            self.payload['name']+='-'+payload['inport']
            self.payload['ingress-port']=payload['inport'] 
        if payload['ethtype']:
            self.payload['name']+='-'+payload['ethtype']
            self.payload['ether-type']=payload['ethtype']
        if payload['protocol']:
            self.payload['name']+='-'+payload['protocol']
            self.payload['protocol']=payload['protocol']
        self.payload['priority']=priority
        self.payload['cookie']='1'
        self.payload['active']='true'
        self.payload['ether-type']='2048'

    """Modifies payload action"""
    def set_action(self,action_type,action=None):
        if action_type=='mod-src-ip':
            action_type='set-src-ip'
        elif action_type=='mod-dst-ip':
            action_type='set-dst-ip'
        elif action_type=='mod-src-mac':
            action_type='set-src-mac'
        elif action_type=='mod-dst-mac':
            action_type='set-dst-mac'
        elif action_type=='mod-src-port':
            action_type='set-src-port'
        elif action_type=='mod-dst-port':
            action_type='set-dst-port'
        elif action_type=='mod-vlan-id':
            action_type='set-vlan-id'
        elif action_type=='mod-vlan-priority':
            action_type='set-vlan-priority'
        elif action_type=='mod-strip-vlan':
            action_type='set-strip-vlan'
        elif action_type=='mod-tos-bits':
            action_type='set-tos-bits'
        if action:
            if 'actions' in self.payload:
                self.payload['actions']+=','+action_type+'='+str(action)
            else:
                self.payload['actions']=action_type+'='+str(action)
        else:
            self.payload['action']=action_type+'='
    """In one scenario could be more than one switch"""
    def messagessForAllSwitches(self):
        switches=self.getSwitchesInScenario()
        for switch in switches:
            temporal_payload=self.payload.copy()   #Make a copy of the dictionary payload to avoid modifications of the object 
            temporal_payload['switch']=switch      
            temporal_payload['name']=self.payload['name']+'-'+switch   #Each flow needs a unique name
            self.payload_list.append(temporal_payload)
    """Sends message to a specific switch"""
    def messageForSpecificSwitch(self,switch):
        self.payload['switch']=switch
        self.payload['name']+='-'+switch    #Each flow needs a unique name
        self.payload_list=self.payload

