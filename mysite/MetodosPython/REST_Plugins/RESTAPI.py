#!usr/bin/python
#------------------------------------------------------------------
#Filename:RESTAPI.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
    REST API. 
    Handles general access to REST APIs
"""
#------------------------------------------------------------------
import httplib
import json

class RESTAPI(object):
    """
    Manages static flows to floodlight controller via API REST
    """
    def __init__(self, controller):
        self.controller=controller
        self.headers={
            'Content-type': 'application/json',
            'Accept': 'application/json',
            }
    def setPath(self,path):
        """
        Sets path for the REST resource
        """
        self.path = path
    def post(self,payload_list=None):
        """
        Sets POST request
        """
        ret = self.send('POST',payload_list)
        return ret
    def remove(self,payload_list):
        """
        Sets DELETE request
        """
        ret = self.send('DELETE',payload_list)
        return ret
    def get(self,payload_list=None):
        """
        Sets GET request
        """
        ret = self.send('GET',payload_list)
        return ret
    def send(self, action,payload=None):
        """
         Acces to de floodlight REST API 
        """
        if(payload):
            body = json.dumps(payload)
        else:
            body=''
        conn = httplib.HTTPConnection(self.controller.controller_ip, 
                                    self.controller.filter_port)
        conn.request(action, 
                    self.path, 
                    body, 
                    self.headers)
        response = conn.getresponse()
        ret = (response.status, response.reason, response.read())
        conn.close()
        return ret

