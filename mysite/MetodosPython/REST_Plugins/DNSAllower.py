
#!usr/bin/python
#------------------------------------------------------------------
#Filename:NACApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:06/06/2014
#------------------------------------------------------------------
"""
	DNS Allower. 
	Grants DNS queries for all devices
"""
#------------------------------------------------------------------