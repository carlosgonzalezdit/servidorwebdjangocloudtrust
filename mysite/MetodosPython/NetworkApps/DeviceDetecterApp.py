#!usr/bin/python
#------------------------------------------------------------------
#Filename:DeviceDetecterApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Device Detecter. 
	Network app in charge of managing new devices info
"""
#------------------------------------------------------------------

from MetodosPython.DBInternalAccess import DBAccess
from MetodosPython.ControllerInfo import controlDevices
from MetodosPython.ControllerDispatcher import ControllerDispatcher
from MetodosPython.NetworkServices.INewDevice import INewDeviceService
from zope.interface import implements


"""---------------------------------------"""
"""APP THAT RECEIVE INFO FROM THE CONTROLLER """

class DeviceDetecter(object):
	"""docstring for DeviceDetecter"""
	implements(INewDeviceService)
	def __init__(self):
		self.ControllerDispatcher=ControllerDispatcher()
		self.initialConfig()

	def newDeviceDetected(self,message):
		"""
		Called each time a new device is detected
		"""
		self.save(message)
	def initialConfig(self):
		"""
		Specifies he neccesary initial configuration for notification devices
		"""
		mac_list=controlDevices().getEventSenderList()
		print mac_list
		for mac in mac_list:
			self.NoDeviceNotification(mac)
	def NoDeviceNotification(self,mac):
		"""(str)->void
		Notifies via REST a non notificable device to the controller
		"""
		self.ControllerDispatcher.set_payload('DeviceDetecter',srcmac=mac)
		self.ControllerDispatcher.addDeviceToPktInServiceBlackList()
	def save(self,dictionary):
		"""(dict)->void
		Saves info in the db
		"""
		if controlDevices().isControlDevice(dictionary['src mac'])==0:
			print dictionary
			date=dictionary['date']
			DBAccess().saveLog(time=date,
						message=dictionary['message'],
						information=dictionary['src mac']) #Register the event in the log system
 			p=DBAccess().getNetworkElementByMAC(MAC_Address=dictionary['src mac'])
 			if p=='Does not exist': #If network device does not exist, create it
				DBAccess().createNetworkElement(MAC_Address=dictionary['src mac'],
										IP_Address=dictionary['ip'],
										transfered_bytes=0,
										previous_bytes=0,
										previous_previous_bytes=0,
										time=0,
										offset_bytes=0)
				print "salvado"
 			else:
				if p.IP_Address==dictionary['ip']:	#Device IP has not changed
					print "Ya exise"
				else:								#Device IP has changed
					DBAccess().updateNetworkElementIP(p,dictionary['ip'])
					print "cambia ip dispositivo"+ dictionary['ip']
