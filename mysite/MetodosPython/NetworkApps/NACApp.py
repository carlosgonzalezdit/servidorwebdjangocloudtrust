
#!usr/bin/python
#------------------------------------------------------------------
#Filename:NACApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	NAC. 
	Provides network access control to the network
"""
#------------------------------------------------------------------
from MetodosPython.ControllerDispatcher import ControllerDispatcher
from MetodosPython.ControllerInfo import captivePortal
from MetodosPython.DBInternalAccess import DBAccess
from MetodosPython.ControllerInfo import controlDevices
from MetodosPython.NetworkApps.switchApp import switch
from MetodosPython.NetworkApps.DNSAllowerApp import DNSAllower
import datetime
import time
import threading
from MetodosPython.Config.WebServerFinder import WebServerFinder
from django.utils.timezone import utc
from zope.interface import implements
from MetodosPython.NetworkServices.IPktInService import IPktInService
class NAC(object):
	"""
	Intelligence of the NAC application
	"""
	implements(IPktInService)
	def __init__(self):
		self.ControllerDispatcher=ControllerDispatcher()
		self.captive_portal=captivePortal()
		self.getWebServerPort()
		self.priority='150'
		#Nac add control devices to switch because he is in charge
		switch_commodity=switch()
		for srcmac in controlDevices().getControlDevicesMac():
			switch_commodity.addDevice(srcmac)
		#We need to allow DNS queries when working with NAC
		DNSAllower().allow()
		#Starts  session handler
		session=SessionHandler(self)
		session.start()
	def pktInMessage(self,message):
		"""
		Implementation of the IPktInService interface
		Will be called when a pktin arrives from the controller
		"""
		self.main(message)
	def getWebServerPort(self):
		"""()->void
		Searches the web server port
		"""
		self.web_server_port=WebServerFinder().findWebServerPort()
		print self.web_server_port
	def registration(self,username,srcmac):
		"""(str,str)->void
		Links a device with an user
		"""
		DBAccess().registerDeviceForUser(username,srcmac)	#Save in db de device for the use
		SessionHandler(self).assignSessionTime(username)		#Assigns session time to the user
	
	def allowedDevice(self,srcmac):	
		"""(str)->void
		Device is authenticated
		"""
		self.removeCaptivePortalRules(srcmac)				#Remove Captive Portal rules for user
		switch().addDevice(srcmac)

	def installCaptivePortalRules(self,srcmac,dstmac,srcip,dstip,srcport,dstport,ethtype,protocol,inport):
		"""(str,str,str,str,int,int,int)->void
		Installs captive portal rules
		"""
		#when a device is not auth must be redirected to captive portal
		#Make NAT	
		self.ControllerDispatcher.set_payload('NAC',
											srcip=srcip,
											dstip=dstip,
											srcport=srcport,
											dstport=dstport,
											ethtype=ethtype,
											protocol=protocol,
											priority=self.priority
											)
		self.ControllerDispatcher.sendOrder('set',
											info=[{'mod-dst-mac':self.captive_portal.mac},
												{'mod-dst-ip':self.captive_portal.ip},
												{'mod-dst-port':self.captive_portal.port},
												{'output':self.web_server_port}])
		
		self.ControllerDispatcher.set_payload('NAC',
											srcip=self.captive_portal.ip,
											dstip=srcip,
											srcport=self.captive_portal.port,
											dstport=srcport,
											priority=self.priority,
											ethtype=ethtype,
											protocol=protocol)
		self.ControllerDispatcher.sendOrder('set',
											info=[{'mod-src-mac':dstmac},
												{'mod-src-ip':dstip},
												{'mod-src-port':dstport},
												{'output':inport}])
		db=DBAccess()
		if db.nacEntryExists(srcmac,dstmac,srcip,dstip,srcport,dstport,ethtype,protocol,inport)==1:
			db.saveInfoNACDevice(srcmac,dstmac,srcip,dstip,srcport,dstport,ethtype,protocol,inport)
	def removeCaptivePortalRules(self,srcmac):
		"""(str)->void
		Uninstall Captive portal rules
		"""
		db=DBAccess()
		entries=db.getNACEntriesForDevice(srcmac)
		for row in entries:
			#Once a device is authenticated, remove the captive Portal redirection
			self.ControllerDispatcher.set_payload('NAC',
												srcip=row.srcip,
												dstip=row.dstip,
												srcport=row.srcport,
												dstport=row.dstport,
												ethtype=row.ethtype,
												protocol=row.protocol,
												priority=self.priority)
			self.ControllerDispatcher.deleteOrder()
			self.ControllerDispatcher.set_payload('NAC',
												srcip=self.captive_portal.ip,
												dstip=row.srcip,
												srcport=self.captive_portal.port,
												dstport=row.srcport,
												ethtype=row.ethtype,
												protocol=row.protocol,
												priority=self.priority)
			self.ControllerDispatcher.deleteOrder()
		db.deleteEntriesForDevice(srcmac)
	def deauthenticateDevice(self,srcmac):
		"""(str)->void
		Device goes from authenticated state to NotAuthenticated
		"""
		switch().deleteDevice(srcmac)
		#db=DBAccess()	
		#db.undefineRegisteredDevice(srcmac)
	def captivePortalRulePort80(self,dictionary):
		#Only install rules for traffic to http ports
		if dictionary['dst port']=='80':
			print "device not authenticated: "+str(dictionary['src mac'])+"--->"+str(dictionary['dst mac'])
			self.installCaptivePortalRules(dictionary['src mac'],		#If not, put it as not authenticated
									dictionary['dst mac'],
									dictionary['src ip'],
									dictionary['dst ip'],
									dictionary['src port'],
									dictionary['dst port'],
									dictionary['eth type'],
									dictionary['protocol'],
									dictionary['inport'])
	def main(self,dictionary):
		"""(dict)->void
		Main loop of the NAC app
		"""
		if (controlDevices().isControlDevice(dictionary['src mac'])==0)&(controlDevices().isCaptivePortal(dictionary['src mac'])==0)&(controlDevices().isBroadcast(dictionary['dst mac'])==0)&(controlDevices().ishostif(dictionary['src ip'])==0)&(controlDevices().ishostif(dictionary['dst ip'])==0):
			db=DBAccess()
			registered=db.userRegisteredForMAC(dictionary['src mac'])	#Check if device is registered
			#Device not associated with any user
			if registered==1:
				self.captivePortalRulePort80(dictionary)
				#Device associated with a Non validated user
			elif registered.user.groups.all()[0].name == 'NotValidated':
				self.captivePortalRulePort80(dictionary)
			else:
				print "Device authenticated : " +str(dictionary['src mac'])
				self.allowedDevice(dictionary['src mac'])
				#self.installRulesForGroup(registered.user.username)
		if controlDevices().ishostif(dictionary['src ip'])==1:
			switch().addDevice(dictionary['src mac'])
	def installRulesForGroup(self,username):
		"""
		Installs specified rules for the user
		"""
		user=DBAccess().getUserByUsername(username)
		register=DBAccess().getRegisterForUser(user)
		if register is not 1:
			#If there something registered
			group=user.groups.all()[0].name		#Asign capabilites based on their role 
			if group=='Admin':
				print "Instalo reglas de Administrador"
			elif group=='Guest':
				SessionHandler(self).assignSessionTime(username)
				for device in register.network_element.all():
					#self.registration(username,device.MAC_Address)
					self.allowedDevice(device.MAC_Address)        #Assigns session time to the user
				print "Instalo reglas de Guest"
			elif group=='User':
				print "Instalo reglas de User"
				for device in register.network_element.all():
					#self.registration(username,device.MAC_Address)
					self.allowedDevice(device.MAC_Address)
			elif group=='NotValidated':
				for device in register.network_element.all():
					self.deauthenticateDevice(device.MAC_Address)
class SessionHandler(threading.Thread):
	"""
	Handles sessions for users in the system
	"""
	def __init__(self,NAC):
		threading.Thread.__init__(self)
		self.sessionTime=30
		self.chek_interval=15
		self.NAC=NAC
	def assignSessionTime(self,username):
		""" () --> int
		Assigns sessionTime for user
		"""
		#Get User
		user=DBAccess().getUserByUsername(username)
		#Based on group different actions will be performed
		register=DBAccess().getRegisterForUser(user)
		if register == 1:
			pass
		else:
			DBAccess().updateRegisterDateForUser(register,datetime.datetime.now().replace(tzinfo=utc))
	def run(self):
		"""() -->void
		Main loop of session handler
		"""
		while 1:
			#Checks every minute
			time.sleep(self.chek_interval)
			#Check if registered users sessions expired
			register=DBAccess().getRegister()
			for element in register:
				if self.checkElementSession(element):
					print "------Session expired for user----- :->"+ element.user.username 
					#Deauthenticate every device asociated and erase register row
					for network_element in element.network_element.all():
						self.NAC.deauthenticateDevice(network_element.MAC_Address)
						print "Quitando privilegion a : "
						print network_element.MAC_Address
						DBAccess().undefineRegisteredDevice(network_element.MAC_Address)
					#DBAccess().removeRegisterElement(element)
	def getSessionTime(self,username):
		"""(Register)->int
		gets session time in sec for a registered element
		"""
		user=DBAccess().getUserByUsername(username)
		group=user.groups.all()[0].name
		if group=='Guest':
			return 30
		elif group=='User':
			return 0 #Does not need expiration time
		elif group == 'Admin':
			return 0 #Does not need expiration time
		elif group == 'NotValidated':
			return 0 #Does not need expiration time
	def checkElementSession(self,registered_element):
		"""(Register)->boolean
		Checks if a registered element session is over
		"""
		session_time=self.getSessionTime(registered_element.user.username)
		print "----Session time----"
		print session_time
		return self.hasSessionExpired(registered_element.registered_date,session_time)
	def hasSessionExpired(self,date,expiration_time):
		"""(datetime)->boolean
		Checks if date differs from actual date more than expiration_time
		"""
		if date==None:
			return False
		actual_time=datetime.datetime.now().replace(tzinfo=utc)
		difference=actual_time-date
		#Session time is over or group does not have session time
		if (difference.total_seconds()<expiration_time)|(expiration_time==0):
			return False
		else:
			return True