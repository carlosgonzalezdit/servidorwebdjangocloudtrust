#!usr/bin/python
#------------------------------------------------------------------
#Filename:DNSAllowerApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:06/06/2014
#------------------------------------------------------------------
"""
	DNS Allower. 
	Grants DNS queries for all devices
"""
#------------------------------------------------------------------
from MetodosPython.Config.WebServerFinder import WebServerFinder
from MetodosPython.ControllerDispatcher import ControllerDispatcher

class DNSAllower(object):
	"""Assures all devices can make DNS queries"""
	def __init__(self):
		self.ControllerDispatcher=ControllerDispatcher()
		self.priority='160'
	def getrouterPort(self):
		"""()->int
		Finds and return the router port number
		"""		
		return WebServerFinder().findPort('router-e1')
	def allow(self):
		"""()->void
		Enables DNS requests
		"""
		self.router_port= self.getrouterPort()
		self.ControllerDispatcher.set_payload('DNSAllower',
											dstport='53',
											ethtype='0x0800',
											protocol='0x11',
											priority=self.priority)
		self.ControllerDispatcher.sendOrder('set',info=[{'output':self.router_port}])