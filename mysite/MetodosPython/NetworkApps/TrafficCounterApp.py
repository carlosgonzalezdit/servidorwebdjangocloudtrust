#!usr/bin/python
#------------------------------------------------------------------
#Filename:TrafficCounterApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Traffic Counter. 
	Manages the traffic information received
"""
#------------------------------------------------------------------
import os,sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
from GestionRed.models import ElementoDeRed, HistoricoDeConsumo, LogDeLaRed
from MetodosPython.NetworkApps.HistoryApp import History
from MetodosPython.DBInternalAccess import DBAccess
from MetodosPython.ControllerInfo import controlDevices
from zope.interface import implements
from MetodosPython.NetworkServices.ITrafficCounterService import ITrafficCounterService
"""APP THAT RECEIVE INFO FROM THE CONTROLLER """
class TrafficCounter(object):
	"""docstring for Traffic"""
	implements(ITrafficCounterService)
	def __init__(self):
		pass
	def trafficInfoMessage(self,message):
		"""
		Implement the interface method
		This method will be called when a new message with consumed data arrive
		"""
		self.save(message)
	"""Saves traffic info"""
	def save(self,dictionary):
		date=dictionary['date']
		del dictionary['date']
		del dictionary['message_type']
		for key,device in dictionary.items():
			if (controlDevices().isControlDevice(device['src mac'])|controlDevices().isCaptivePortal(device['src mac']))==0:		
				DBAccess().saveLog(time=date,
							message='Traffic used',
							information=device['src mac']+':'+str(device['bytes']))
				network_element=DBAccess().getNetworkElementByMAC(MAC_Address=device['src mac'])
 				if network_element=='Does not exist': 
					print "No existe la direccion MAC"
					print device['src mac']
				else:
					network_element.time=date
					#Offset takes into acount resets in the controller
					print "------"+device['src mac']+"-----"
					print " Bytes : "+str(device['bytes'])+"   "
					print " Prevoius Bytes : "+str(network_element.transfered_bytes)+"   "
					print " Previous offset :"+str(network_element.offset_bytes)
					if (device['bytes']+int(network_element.offset_bytes))<int(network_element.transfered_bytes): #If counters have been reset
						network_element.offset_bytes= network_element.transfered_bytes							#Actual byte count is now de offset
						print "offset:-----------"+str(network_element.offset_bytes)		
					network_element.previous_previous_bytes=network_element.previous_bytes						#Update counters
					network_element.previous_bytes=network_element.transfered_bytes
					network_element.transfered_bytes=int(device['bytes'])+int(network_element.offset_bytes)		#Transfered bytes depend on the offset
					network_element.save()
		DBAccess().updateNetworkElementsDate(date)
		History().saveHistoric()					#Makes a picture of the actual state for the history
