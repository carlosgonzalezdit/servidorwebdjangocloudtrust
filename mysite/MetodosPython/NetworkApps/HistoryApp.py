#!usr/bin/python
#------------------------------------------------------------------
#Filename:HistoryApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	History app. 
	Network app in charge of managing traffic info through time
"""
#------------------------------------------------------------------

from MetodosPython.DBInternalAccess import DBAccess

class History(object):
	"""
	Makes a picture of the actual consumed traffic data and save it
	"""
	def __init__(self):
		pass
	def saveHistoric(self):
		"""
		Saves traffic information into the db
		"""
		elements=DBAccess().getNetworkDevices()
		for element in elements:
			DBAccess().saveHistory(dia=element.time,MAC_Address=element.MAC_Address,consumo=element.transfered_bytes)
