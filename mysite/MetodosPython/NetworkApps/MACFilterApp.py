
#!usr/bin/python
#------------------------------------------------------------------
#Filename:MACFilterApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Mac Filter. 
	Network app to filter network access to devices
"""
#------------------------------------------------------------------
from MetodosPython.ControllerDispatcher import ControllerDispatcher

class MACFilter(object):
	"""docstring for DenyApp"""
	def __init__(self, mac):
		self.mac=mac
		self.priority='145'
	def deny(self):
		"""
		Deny access to network services for a device
		"""
		dispatcher=ControllerDispatcher()
		dispatcher.set_payload("filter",srcmac=self.mac,priority=self.priority)
		dispatcher.sendOrder('drop')
	def enable(self):
		"""
		Enables access to network services for a device
		"""
		dispatcher=ControllerDispatcher()
		dispatcher.set_payload("filter",srcmac=self.mac,priority=self.priority)
		dispatcher.deleteOrder()




