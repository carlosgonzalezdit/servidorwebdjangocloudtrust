#!usr/bin/python
#------------------------------------------------------------------
#Filename:switchApp.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:26/06/2014
#------------------------------------------------------------------
from MetodosPython.ControllerDispatcher import ControllerDispatcher
class switch(object):
	"""
	App implements a switch
	"""
	def __init__(self):
		self.ControllerDispatcher=ControllerDispatcher()
	def addDevice(self,srcmac):
		"""(str)->void
		Add a device to the switch commodity
		"""
		self.ControllerDispatcher.set_payload('switch',srcmac=srcmac)
		self.ControllerDispatcher.addDeviceToSwitchCommodity()
	def deleteDevice(self,srcmac):
		"""(str)->void
		Deletes a device to the switch commodity
		"""
		self.ControllerDispatcher.set_payload('switch',srcmac=srcmac)
		self.ControllerDispatcher.deleteDeviceToSwitchCommodity()