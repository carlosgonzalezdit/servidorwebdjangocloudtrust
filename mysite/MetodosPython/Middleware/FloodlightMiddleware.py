#!usr/bin/python
#------------------------------------------------------------------
#Filename:FloodlightMiddleware.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Floodlight Middleware. 
	Software that acts between the webserver and the controller to adapt data and information
"""
#------------------------------------------------------------------


from MetodosPython.REST_Plugins.FloodlightRESTAccess import FloodlightRESTAccess
from MetodosPython.REST_Plugins.RESTAPI import RESTAPI
from MetodosPython.ControllerInfo import ControllerInfo,controlDevices
from MetodosPython.DBInternalAccess import DBAccess
import json,math, time,socket
import threading

"""Class to simulate a periodic statistics notification system for floodlight"""
class HistoryMiddleware(threading.Thread):
	"""docstring for HistoryMiddleware"""
	def __init__(self):
		threading.Thread.__init__(self)
		self.controller=ControllerInfo()
		self.timer=30
	"""Returns json with the statistics for all the discovered switches"""
	def retrieveFloodlightFlowStatistics(self):
		rest=RESTAPI(self.controller)
		rest.setPath(FloodlightRESTAccess().switchstatisticsPath())
		info=rest.get()[2]
		return json.loads(info)
	"""Formats statistics for the Notification system"""
	def formatStatisticsInfo(self, statistics):
		devices_list=DBAccess().networkElements()
		devices_stats=[]
		for ip,mac in devices_list:
			received_bytes=0
			transmitted_bytes=0
			#For every switch in the scenario
			for switch in statistics:
				#Each switch can have several flows
				for table in statistics[switch]:
					if table['priority']==100:					#Priority 100 is used for statistisc flows
						match=table['match']
						#Collects data for every device in the network
						if mac==match['dataLayerDestination']:
							received_bytes+=table['byteCount']
						if mac==match['dataLayerSource']:
							transmitted_bytes+=table['byteCount']
			devices_stats.append((mac,received_bytes,transmitted_bytes))
		return devices_stats
	"""Sends statistics info to the notification system"""
	def sendStatisticsToReceiver(self,info):
		date=int(math.floor(time.time()))
		i=1
		message=dict(date=date,message_type='trafficCounter')	#Value to identify the message
		for mac,received,transmitted in info:
			if controlDevices().isControlDevice(mac)==0:		
				a={'src mac':mac,'bytes':received+transmitted}			#Puts together all bytes
				string='device'+str(i)
				message[string]=a
				i+=1
		s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		s.connect((self.controller.web_server,					# Connect to server
					int(self.controller.traffic_port)))
		s.sendall(json.dumps(message))			# Send data
		s.close()
	"""Main method for the thread"""	
	def run(self):
		while 1:
			time.sleep(self.timer)		#Time interval between sending stats
			stats=self.retrieveFloodlightFlowStatistics()					#Querys the stats
			self.sendStatisticsToReceiver(self.formatStatisticsInfo(stats))	#Formats and sends the stats info
