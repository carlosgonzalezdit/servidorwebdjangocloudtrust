#!usr/bin/python
#------------------------------------------------------------------
#Filename:NetworkFuncitions_RESTAPI.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
    Network Apps REST API. 
    Provides a REST API for the network apps
"""
#------------------------------------------------------------------
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
from MetodosPython.DBInternalAccess import DBAccess
from MetodosPython.NetworkApps.MACFilterApp import MACFilter
from MetodosPython.NetworkApps.NACApp import NAC
from MetodosPython.NetworkApps.NACApp import SessionHandler
import json


"""WEB SERVICES """  
"""Shows the log for the web page"""  
def log(request):
    if request.user.is_authenticated():
        log=DBAccess().log(request)
        t=get_template('log.html')
        html=t.render(Context({"log_list":log}))
        return HttpResponse(html)
    else:
        return HttpResponseRedirect('/')
"""Shows the history for the web page"""  
def history(request):
    if request.user.is_authenticated():
        t=get_template('historico_consumo.html')
        html=t.render(Context({"historico":DBAccess().historyMatrix()}))
        return HttpResponse(html)
    else:
        return HttpResponseRedirect('/')
"""Shows devices and characteristics for the web page"""  
def search_form(request):
    if request.user.is_authenticated():
        t=get_template('search_form.html')
        device_tuple,user_list=prepareDataForSearchForm()
        html=t.render(Context({"device_dict":device_tuple,"user_list":user_list}))
        return HttpResponse(html)
    else:
        return HttpResponseRedirect('/')
def authToUser(request):
    user=request.GET['user']
    mac=request.GET['mac']
    NAC().registration(user,mac)
    #NAC().allowedDevice(mac)
    NAC().installRulesForGroup(user)
    return search_form(request)
def changeUserGroup(request):
    if request.user.is_authenticated():
        user_name=request.GET['user']
        group_name=request.GET['group']
        #Change the user group
        DBAccess().changeUserGroup(user_name,group_name)
        #Update session time information
        NAC().installRulesForGroup(user_name)
        template=get_template('settings.html')
        html=template.render(Context({"user_list":DBAccess().getUsers(),"group_list":DBAccess().getGroups(),"register_list":DBAccess().getRegister()}))
        return HttpResponse(html)
    else:
        return HttpResponseRedirect('/')
"""------------------------------------------------"""
"""ACTIONS SUPPORTED BY PYRESONANCE/FLOODLIGHT IN CONTROLLER"""

"""State machine to filter a device"""
def filter(request):
    q=request.GET['q']
    MACFilter(mac=q).deny()
    return search_form(request)
def not_filter(request):
    q=request.GET['q']
    MACFilter(mac=q).enable()
    return search_form(request)

"""------------------------------------------------"""
"""API FOR EXTERNAL DEVICES"""    

"""Shows devices in the network"""
def ipMobile(request):
    return HttpResponse(json.dumps(DBAccess().JSONnetworkElements()),content_type="application/json")
"""Shows log"""
def logMobile(request):
    return HttpResponse(json.dumps(DBAccess().networkLog()),content_type="application/json")
"""Filter for devices"""
def enableMobile(request):
    q=request.GET['q']
    MACFilter(mac=q).enable()
    return HttpResponse("habilitado")
"""Filter for devices"""
def filterMobile(request):
    q=request.GET['q']
    MACFilter(mac=q).deny()


"""--------------------------"""
def prepareDataForSearchForm():
    device_dict=DBAccess().JSONnetworkElements()
    rango=range(0,len(device_dict),2)
    device_tuple=[]
    device_list=[]
    max_traffic=50000
    user_list=DBAccess().getUsers()
    for key , element in  device_dict.items():
        consumed_percentage=100*float(element['transfered_bytes'])/max_traffic
        element['consumed_percentage']=consumed_percentage
        device_list.append(element)
    for i in rango:
        p=i+1
        try:
            device_tuple.append((device_list[i],device_list[p]))
        except:
            device_tuple.append((device_list[i],"vacio"))
    return device_tuple,user_list