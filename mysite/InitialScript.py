
#!usr/bin/python
#------------------------------------------------------------------
#Filename:InitialScript.py
#Author:Carlos Gonzalez Merino
#email:carlosgonza@dit.upm.es
#Date:22/04/2014
#------------------------------------------------------------------
"""
	Initial Script.
	Starts all the engine of the web server
"""
#------------------------------------------------------------------
from MetodosPython.ControllerReceiver import NotificationReceivers
import commands
import time
from MetodosPython.ControllerDispatcher import ControllerDispatcher

"""Starts all the necessary things for the web server"""
def Inicio():
    time.sleep(20)
    dispatcher=ControllerDispatcher()
    dispatcher.startDispatcher()					#Starts the dispatcher
    conn=NotificationReceivers(dispatcher.controller)	#Starts notifications handlers
    conn.start()
    print "Servidor arrancado"
    status,output=commands.getstatusoutput('sudo python /home/ServidorWebDjango/servidorwebdjangocloudtrust/mysite/manage.py runserver 0.0.0.0:3001')
Inicio()
